package com.org.greatLearning_Week3;
import java.util.*;
//import java.util.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;



import com.org.greatLearning_Week3.CustomException;
import com.org.greatLearning_Week3.book;
	public class magicbook<T>{
		
		Scanner scanner=new Scanner(System.in);


		HashMap<Integer,book> bookmap=new HashMap<>();
		TreeMap<Double,book> treemap=new TreeMap<>();

		ArrayList<book> booklist=new ArrayList<>();
		//adding books
		public  void addbook(){

			book b= new book();
			System.out.println("Enter book id : ");
			b.setId(scanner.nextInt());

			System.out.println("Enter book name : ");
			b.setName(scanner.next());

			System.out.println("Enter book price : ");
			b.setPrice(scanner.nextDouble());

			System.out.println("Enter book genre (Enter Autobiography for autobiography genre) : ");
			b.setGenre(scanner.next());

			System.out.println("Enter number of copies sold : ");
			b.setNoOfCopiesSold(scanner.nextInt());

			System.out.println("Enter book status (Enter B for bestselling) : ");
			b.setBookstatus(scanner.next());

			bookmap.put(b.getId(), b);
			System.out.println("Book added successfully");

			treemap.put(b.getPrice(), b);
			booklist.add(b);

		}
		//deleting books
		public void deletebook() throws CustomException{
			if(bookmap.isEmpty()) {
				throw new CustomException("no books are available to delete!!");
			}
			else {
				System.out.println("Enter book id you want to delete : ");
				int id=scanner.nextInt();
				bookmap.remove(id);	
				System.out.println("successfully deleted!!");
			}


		}
		//updating books
		public void updatebook() throws CustomException {
			if(bookmap.isEmpty()) {
				throw new CustomException("No books are available to update!!");
			}else {


				book b= new book();
				System.out.println("Enter book id : ");
				b.setId(scanner.nextInt());

				System.out.println("Enter book name : ");
				b.setName(scanner.next());

				System.out.println("Enter book price : ");
				b.setPrice(scanner.nextDouble());

				System.out.println("Enter book genre :(Enter Autobiography for autobiography genre ");
				b.setGenre(scanner.next());

				System.out.println("Enter number of copies sold : ");
				b.setNoOfCopiesSold(scanner.nextInt());

				System.out.println("Enter book status : (Enter B for bestSelling ");
				b.setBookstatus(scanner.next());

				bookmap.replace(b.getId(), b);
				System.out.println("Book details Updated successfully!!");
			}
		}
		//displaying books
		public void displayBookInfo() throws CustomException {

			if (bookmap.size() > 0) 
			{
				Set<Integer> keySet = bookmap.keySet();

				for (Integer key : keySet) {

					System.out.println(key + " ----> " + bookmap.get(key));
				}
			} else {
				throw new CustomException("BooksMap is Empty!!");
			}

		}
		//counting
		public void count() throws CustomException {
			if(bookmap.isEmpty()) {
				throw new CustomException("book store is empty!!");
			}else

				System.out.println("Number of books present in the store : "+bookmap.size());

		}	
		//autobiography search
		public void autobiography() throws CustomException {
			String bestSelling = "Autobiography";
			if(booklist.isEmpty()) {
				throw new CustomException("Book store is Empty!!");
			}
			else {

				ArrayList<book> genreBookList = new ArrayList<book>();

				Iterator<book> iter=(booklist.iterator());

				while(iter.hasNext())
				{
					book b1=(book)iter.next();
					if(b1.genre.equals(bestSelling)) 
					{
						genreBookList.add(b1);
						System.out.println(genreBookList);
					}
				} 
			}
		}
		//displaying by feature
		public void displayByFeature(int flag) throws CustomException{

			if(flag==1)//low to high
			{
				if (bookmap.size() > 0) 
				{
					Set<Double> keySet = treemap.keySet();
					for (Double key : keySet) {
						System.out.println(key + " ----> " + treemap.get(key));
					}
				} else {
					throw new CustomException("book store is empty is Empty!!");
				}
			}
			if(flag==2) //high to low		
			{
				Map<Double, Object> treeMapReverseOrder= new TreeMap<>(treemap);
				// putting values in navigable map
				NavigableMap<Double, Object> nmap = ((TreeMap<Double, Object>) treeMapReverseOrder).descendingMap();

				System.out.println("Book Details : ");

				if (nmap.size() > 0) 
				{
					Set<Double> keySet = nmap.keySet();
					for (Double key : keySet) {
						System.out.println(key + " ----> " + nmap.get(key));
					}
				}else{
					throw new CustomException("book store is Empty!!");
				}

			}

			if(flag==3) //best selling
			{
				String bestSelling = "B";
				ArrayList<book> genreBookList = new ArrayList<book>();

				Iterator<book> iter=booklist.iterator();

				while(iter.hasNext())
				{
					book b=(book)iter.next();
					if(b.getBookstatus().equals(bestSelling)) 
					{
						genreBookList.add(b);

					}

				}
				if(genreBookList.isEmpty())
				{
					throw new CustomException("no best selling books are available!!");
				}
				else
					System.out.println("best selling books : "+genreBookList);

			}
		}	
	}
